'use strict';

exports.handler = (event, context, callback) => {
    let request = event.Records[0].cf.request;
    const redirectResponse = {
        status: 307,
        statusDescription: 'Temporary Redirect', 
        headers: {
        'location': [{
            key: 'Location',
            value: 'https://tickets.rugbyworldcup.com',
        }],
        'cache-control': [{ // Set cache 1h
            key: 'Cache-Control',
            value: "max-age=60"
        }],
        }, 
    };

    callback(null, redirectResponse);
}
