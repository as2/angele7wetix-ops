## Setup venv

## Tests
 test local avec :
   $ python-lambda-local -f lambda_handler wrc2023-asg-event.py wrc2023-asg-event.json

## Livraison

 Livraison avec :
   $ zip wrc2023-asg-event.zip wrc2023-asg-event.py
   $ aws lambda update-function-code --function-name wrc2023-asg-event --zip-file fileb://wrc2023-asg-event.zip