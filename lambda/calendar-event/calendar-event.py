# test local avec :
#   $ python-lambda-local -f lambda_handler wrc2023-asg-event.py wrc2023-asg-event.json
# Livraison avec :
#   $ zip wrc2023-asg-event.zip wrc2023-asg-event.py
#   $ aws lambda update-function-code --function-name wrc2023-asg-event --zip-file fileb://wrc2023-asg-event.zip
#

def slack_post(title, sectiontitle, fields):
    from slack_webhook import Slack

    webhooks = [
        {"url": "https://hooks.slack.com/services/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", "channel": "#opsdeploy", "username": "CalendarEvent" },
        {"url": "https://hooks.slack.com/services/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", "channel": "#rwc2023-alertes", "username": "ASGEvent" }
        ]
    
    #for key in ['AutoScalingGroupName', 'Event', 'Time', 'Cause', 'EC2InstanceId']:
    #    notif_fields.append({
    #        "type": "mrkdwn",
    #        "text": "*"+key+":*\n" + message[key]
    #    })

    slack_attachements = [{
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": sectiontitle
                }
            },
            {
                "type": "section",
                "fields": fields
            }
        ]
    }]

    for w in webhooks:
        slack = Slack(url=w["url"])
        slack.post(text=title, attachments=slack_attachements,
                    channel=w["channel"], username=w["username"])


def lambda_handler(event, context):
    import boto3
    import json
    import os

    return_message="OK"

    print ("Current event : " + json.dumps(event, indent="  ") )

    for r in event["Records"]:
        if r["Sns"]:
            message = json.loads(r["Sns"]["Message"])
            print ("Current Message : " + json.dumps(message, indent="  ") )
            notifs = []

            try:
                payload = json.loads(message["description"])
                if payload["AutoScalingGroups"]:
                    for asg in payload["AutoScalingGroups"]:
                        asg_client = boto3.client('autoscaling', region_name='eu-west-3')
                        current_asg = asg_client.describe_auto_scaling_groups(
                            AutoScalingGroupNames=[asg["AutoScalingGroupName"]]
                        )
                        if "MaxSize" not in asg:
                            if asg["MinSize"] > current_asg["AutoScalingGroups"][0]["MaxSize"]:
                                asg["MaxSize"]=asg["MinSize"]
                            else:
                                asg["MaxSize"]= current_asg["AutoScalingGroups"][0]["MaxSize"]

                        print (json.dumps(asg, indent="  "))

                        response = asg_client.update_auto_scaling_group(
                            AutoScalingGroupName=asg["AutoScalingGroupName"],
                            MinSize=asg["MinSize"],
                            MaxSize=asg["MaxSize"],
                        )
                        print (response)
                        notifs.append({ "type": "mrkdwn","text": "```"+ json.dumps(asg, indent="  ") +"```" })
                
                if payload["DBClusters"]:
                    for db in payload["DBClusters"]:
                        print (json.dumps(db, indent="  "))

                        rds_client = boto3.client('rds', region_name='eu-west-3')
                        response = rds_client.modify_db_cluster(
                            DBClusterIdentifier=db['DBClusterIdentifier'],
                            ScalingConfiguration=db['ScalingConfigurationInfo']
                        )
                        print (response)
                        notifs.append({ "type": "mrkdwn","text": "```"+ json.dumps(db, indent="  ") +"```"  })

                if len(notifs):
                    slack_post("Calendar Event", message["summary"], notifs)

            except json.JSONDecodeError:
                slack_post("Error Event", "Cant decode calendar description", [{ "type": "mrkdwn","text": "```"+ json.dumps(message, indent="  ") +"```" }])

    return {
        'message': return_message
    }
