import json
import boto3
import datetime
import logging
import urllib

logger = logging.getLogger()
logger.setLevel(logging.INFO)

date_handler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, (datetime.datetime, datetime.date))
    else None
)

def get(event, context):
    service='cloudwatch'
    client = boto3.client(service)
    #body = json.loads(event['body'])

    logger.info('New event{}'.format(event))

    # aws cloudwatch describe-alarms

    output = {}
    if "pathParameters" in event:
        if "alarm" in event["pathParameters"]:
            output = client.describe_alarms(
                AlarmNames=[
                    urllib.parse.unquote(event["pathParameters"]["alarm"]) ],
                )

    logger.info(service + ' describe_alarms Response {}'.format(output))

    if "MetricAlarms" not in output :
        response = { "statusCode": 502, "body": {"reason": "Bad response from "
            + service} }
    else:
        alarm = output["MetricAlarms"][0]
        response = {
            "statusCode": 200,
            "body": json.dumps( {
                "AlarmName": alarm["AlarmName"],
                "AlarmDescription": alarm["AlarmDescription"],
                "StateValue": alarm["StateValue"],
                })
        }

    return response

