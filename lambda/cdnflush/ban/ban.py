import json
import boto3
import datetime
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

date_handler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, (datetime.datetime, datetime.date))
    else None
)

def post(event, context):
    client = boto3.client('cloudfront')
    body = json.loads(event['body'])

    logger.info('New event{}'.format(event))

    # Check input
    if "urls" not in body :
        response = { "statusCode": 403, "body": {"reason": "wrong_arguments"} }
        logger.info('Result {}'.format(response))
        return response

    urls = body["urls"]
    if type(urls)!=list:
        urls=list(urls)
    
    InvalidationBatch = {'Paths': {'Quantity': len(urls), 'Items': urls},  'CallerReference':  datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f') }
    logger.info('Cloudfront Invalidation Batch {}'.format(InvalidationBatch))

    output = {}
    if "pathParameters" in event:
        if "distribution" in event["pathParameters"]:
            output = client.create_invalidation( 
                DistributionId=event["pathParameters"]["distribution"], 
                InvalidationBatch=InvalidationBatch
                )

    logger.info('Cloudfront Invalidation Batch {}'.format(output))

    if "Invalidation" not in output :
        response = { "statusCode": 502, "body": {"reason": "Bad response from Cloudfront"} }
    else:
        response = {
            "statusCode": 200,
            "body": json.dumps(output["Invalidation"], default=date_handler)
        }

    return response
