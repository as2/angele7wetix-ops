# test local avec :
#   $ python-lambda-local -f lambda_handler wrc2023-asg-event.py wrc2023-asg-event.json
# Livraison avec :
#   $ zip wrc2023-asg-event.zip wrc2023-asg-event.py
#   $ aws lambda update-function-code --function-name wrc2023-asg-event --zip-file fileb://wrc2023-asg-event.zip
#

def lambda_handler(event, context):
    import boto3
    import json
    from slack_webhook import Slack
    import os

    return_message="OK"

    print ("Current event : " + json.dumps(event, indent="  ") )

    ec2_filter_tag="rwc2023wtxtmc-master-prod"
    projet="RWC2023TMC"

    webhooks = [
        {"url": "https://hooks.slack.com/services/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", "channel": "#opsdeploy", "username": "ASGEvent" },
        ]

    for r in event["Records"]:
        if r["Sns"]:
            message = json.loads(r["Sns"]["Message"])
            print ("Current Message : " + json.dumps(message, indent="  ") )

            # lMise en forme de l'alerte Slack
            notif_fields=[]
            for key in ['AutoScalingGroupName', 'Event', 'Time', 'Cause', 'EC2InstanceId']:
                notif_fields.append({
                                "type": "mrkdwn",
                                "text": "*"+key+":*\n"+ message[key]
                            })

            slack_attachements = [{
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": projet+ " ASG Event : " + message["Description"]
                        }
                    },
                    {
                        "type": "section",
                        "fields": notif_fields
                    }
                ]
            }]

            for w in webhooks:
                slack = Slack(url=w["url"])
                slack.post(text=projet+" - autoscaling ", attachments=slack_attachements, channel=w["channel"], username=w["username"])

            # On cherche une instance pour jouer le document
            instanceId=""
            ec2 = boto3.client('ec2', region_name='eu-west-3')

            ec2_filters = [
                [{
                    'Name': 'tag:Name',
                    'Values': [ec2_filter_tag]
                },
                {
                    'Name': 'instance-state-name',
                    'Values': ['running']
                }]
                ]

            for f in ec2_filters:
                if not instanceId:
                    print ("Filtering instances : "+json.dumps(f) )
                    instances = ec2.describe_instances(Filters=f)
                    for r in instances['Reservations']:
                        for i in r['Instances']:
                            instanceId = i['InstanceId']
                            print (i['InstanceId'] +" -> "+ i['LaunchTime'].strftime("%d/%m/%Y %H:%M:%S"))
                            break


            # Run le document SSM
            if instanceId:
                print ("Elected Instance is "+ instanceId)
                ssm = boto3.client('ssm')
                ssm_message = ssm.send_command(
                    InstanceIds=[instanceId],
                    DocumentName='WRC2023-UpdateLsyncdPool',
                    TimeoutSeconds=600,
                    OutputS3Region='eu-west-3'
                )
                return_message = 'OK'
            else:
                return_message = "NoInstanceFound"


    return {
        'message': return_message
    }
