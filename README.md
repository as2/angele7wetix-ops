# ANGELE7 INFRA AWS #

This README would normally document whatever steps are necessary to get your application up and running.

~/.aws/config

[profile angele]
role_arn = arn:aws:iam::915522761382:role/AngeleAccessRole
source_profile = default
region=eu-west-3

export AWS_PROFILE=angele

sudo PUPPET_ENV="angele7prod" AUTOSIGN_API_KEY="i7yerljcch18yje1af2c4q9o94v467a" bash /home/ubuntu/bootstrap.bash

### Master Front + Hubber ###

aws cloudformation create-stack --stack-name ANGELE-MASTER-20211014 --template-body file://master.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3

### Database ###

aws cloudformation create-stack --stack-name ANGELE-RDS-FRONT-20211014 --template-body  file://databases.json \
--parameters ParameterKey=DBName,ParameterValue=front ParameterKey=DBIdentifier,ParameterValue=angele7-front-1  ParameterKey=DBPassword,ParameterValue=Auh2DSfCLOKyej98MEPWfwreT \
--capabilities CAPABILITY_NAMED_IAM --region eu-west-3

aws cloudformation create-stack --stack-name ANGELE-RDS-HUBBER-20211014 --template-body  file://databases.json \
--parameters ParameterKey=DBName,ParameterValue=front ParameterKey=DBIdentifier,ParameterValue=angele7-hubber-1  ParameterKey=DBPassword,ParameterValue=Auh2DSfCLOKyej98MEPWfwreT \
--capabilities CAPABILITY_NAMED_IAM --region eu-west-3

### Creation Nat Gateway ###

aws cloudformation create-stack --stack-name Angele-NATGW-20211014 --template-body file://nat_gateway.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3

### S3 bucket ###

aws cloudformation create-stack --stack-name ANGELE-S3-BUCKET-20211015 --template-body file://s3_bucket.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3
aws cloudformation create-stack --stack-name ANGELE-S3-US-BUCKET-20211015 --template-body file://s3_us_bucket.json --capabilities CAPABILITY_NAMED_IAM --region us-east-1

### Memcached ###

aws cloudformation create-stack --stack-name ANGELE-MEMCACHED-CUSTO-20211015 --template-body file://memcached_custo.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3

### ASG ###

aws cloudformation create-stack --stack-name ANGELE-ASG-FRONT-20211015 --template-body file://asg_front.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3
update front stack -> aws cloudformation update-stack --stack-name ANGELE-ASG-FRONT-20211015 --template-body file://asg_front.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3
update hubber stack -> aws cloudformation update-stack --stack-name ANGELE-ASG-HUBBER-20211015 --template-body file://asg_hubber.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3

### CDN ### 

aws cloudformation create-stack --stack-name ANGELE-CLOUDFRONT-TICKETS-20211022 --template-body file://cloudfront_tickets.json --capabilities CAPABILITY_NAMED_IAM --region us-east-1
aws cloudformation create-stack --stack-name ANGELE-CLOUDFRONT-ONEWAY-20211022 --template-body file://cloudfront_oneway.json --capabilities CAPABILITY_NAMED_IAM --region us-east-1

### WAF ###

aws cloudformation create-stack --stack-name ANGELE-WAF-20211015 --template-body file://waf.json --capabilities CAPABILITY_NAMED_IAM --region us-east-1

# UPGRADE de l'infra pour MEV #

memcached -> aws cloudformation create-stack --stack-name ANGELE-MEMCACHED-CUSTO-MEV-20211026 --template-body file://memcached_custo_mev.json --capabilities CAPABILITY_NAMED_IAM --region eu-west-3
!! Penser à faire flushall du cache "small" avant de faire le rollback après mev !!
!! echo 'flush_all' | nc ang-el-8op12be7hn1b.acmymi.cfg.euw3.cache.amazonaws.com 11211 !!

rds ->  aws rds modify-db-cluster --db-cluster-identifier angele7-front-1 --scaling-configuration MinCapacity=256,MaxCapacity=256 --apply-immediate (min 16, max 32 en croisière)
        aws rds modify-db-cluster --db-cluster-identifier angele7-hubber-1 --scaling-configuration MinCapacity=256,MaxCapacity=256 --apply-immediate (min 16, max 32 en croisière)

instance -> 20 fronts + 10 hubbers en c5.large

